<?php

class dbApi {
    private $connection;

    public function __construct($host, $name, $password, $dbName) {
        $this->connection = mysqli_connect($host , $name , $password , $dbName);
        if ($this->connection == false) {
            echo 'Cannot connect to the server: ';
            echo mysqli_connect_error();
            exit();
        }
        if (!mysqli_set_charset($this->connection, "utf8")) {
            echo "ERROR while downloading the charset: ";
            echo mysqli_error($this->connection);
            exit();
        }
    }

    public function __destructor() {
        mysqli_close($this->connection);
    }

    public function getUser($login, $password) {
        $ret = mysqli_query($this->connection,
        "SELECT * FROM `user` WHERE `login`='{$login}' AND `password`='{$password}'");

        if (mysqli_num_rows($ret) == 0) {
            return array("error" => "this user not exist");
        }

        return mysqli_fetch_assoc($ret);
    }

    public function changeUserName($login, $password, $newName) {
        mysqli_query($this->connection,
        "UPDATE `user` SET `name`='{$newName}' WHERE `login`='{$login}' AND `password`='{$password}'");
    }

    public function getAllUsers() {
        $ret_pointer = mysqli_query($this->connection,
        "SELECT * FROM  `user`");
        $resArray = [];

        while ($trace = mysqli_fetch_assoc($ret_pointer)) {
            $resArray[] = $trace;
        }

        return $resArray;
    }

}

?>