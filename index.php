<?php

//echo phpinfo();

require_once "./db/dbStuff.php";

// connect to the database
$dataBase = new dbApi("127.0.0.1", "root", "adminpass", "first_test_db");

// connecct to the cache
$mc = new Memcache;
$mc->connect('localhost', 11211)  or die("Memcached connection error!");

// check if we already load array with users
$array_with_users = $mc->get('saved_array');

if (!empty($array_with_users)) {
    // we already load array
    echo "Array was in memcache <br/>";
    printRes($array_with_users);
} else {
    // we need to load array from database
    echo "Array wasn't in cache, so we load it from database <br/>";
    $result = $dataBase->getAllUsers();
    // save this array in cache
    $mc->set('saved_array', $result, false, 30);

    printRes($result);
}

function printRes($result) {
    print_r($result);

    echo "<br/><br/>";
    foreach ($result as $val) {
        echo "{$val['name']} : {$val['login']} <br/>";
    }
}

?>